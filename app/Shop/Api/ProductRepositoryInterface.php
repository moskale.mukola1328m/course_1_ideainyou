<?php

namespace Shop\Api;

use Shop\Model\Product;
use Shop\Service\Collection;

interface ProductRepositoryInterface
{
    public function getById(int $id): Product;
    public function deleteById(int $id): void;
    public function save(Product $product): Product;

    public function getCollection(?array $condition = null): Collection;
}