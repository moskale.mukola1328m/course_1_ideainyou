<?php

namespace Shop\ViewModel;

use Shop\Service\DataObject;

class Pager
{
    const PAGE_SIZE = 3;
    const GET_PARAMETER = 'p';
    private $currentPage;
    private $lastPage;
    private $url;

    public function __construct($currentPage, $totalCount, $url)
    {
        $this->currentPage = (int)$currentPage;
        if ($totalCount % self::PAGE_SIZE) {
            $this->lastPage = intdiv($totalCount, self::PAGE_SIZE) + 1;
        } else {
            $this->lastPage = intdiv($totalCount, self::PAGE_SIZE);
        }
        $this->url = $url;
    }

    public function getParamsForRender(): array
    {
        return [
            'total' => $this->lastPage,
            'current' => $this->currentPage,
            'url' => $this->url
        ];
    }

    public function getLimitForCollection(): array
    {
        return [($this->currentPage - 1) * self::PAGE_SIZE, self::PAGE_SIZE];
    }
}