<?php

namespace Shop\Controller;

use Shop\Model\CategoryRepository;
use Shop\Model\ProductRepository;
use Shop\ViewModel\Pager;

class Category extends AbstractController
{
    private $productRepository;
    private $categoryRepository;

    public function __construct(
        \Shop\Model\AdminLogin $adminLogin,
        ProductRepository $productRepository,
        CategoryRepository $categoryRepository
    ) {
        parent::__construct($adminLogin);
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function execute(\Klein\Request $request, \Klein\Response $response)
    {
        $url = '/category/id/' . $request->param('id') . '/p/';
        $pager = new Pager((int)$request->param(Pager::GET_PARAMETER) ?: 1, $this->productRepository->getTotalCount($request->param('id')), $url);

        return $this->render('categoryPage.html.twig', [
            'category' => $this->categoryRepository->getById($request->param('id'))->getData(),
            'products' => $this->productRepository->getCollection([
                'category_id[=]' => $request->param('id'),
                'LIMIT' => $pager->getLimitForCollection()
            ])->getItemsData(),
            'pager' => $pager->getParamsForRender()
        ]);
    }
}