<?php

namespace Shop\Controller;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

abstract class AbstractController
{
    protected $adminLogin;

    public function __construct(\Shop\Model\AdminLogin $adminLogin)
    {
        $this->adminLogin = $adminLogin;
    }

    abstract public function execute(\Klein\Request $request, \Klein\Response $response);

    protected function render($template, $variables = []) {
        $loader = new FilesystemLoader(__DIR__ . '/../view/templates');
        $twig = new Environment($loader);

        // add info for header
        if (!isset($variables['loginStatus'])) {
            $variables['loginStatus'] = $this->adminLogin->validateAdmin();
        }

        return $twig->render($template, $variables);
    }
}