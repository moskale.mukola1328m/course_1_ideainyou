<?php

namespace Shop\Controller;

use Shop\Model\CategoryRepository;

class Home extends AbstractController
{
    private $categoryRepository;

    public function __construct(
        \Shop\Model\AdminLogin $adminLogin,
        CategoryRepository $categoryRepository
    ) {
        parent::__construct($adminLogin);
        $this->categoryRepository = $categoryRepository;
    }

    public function execute(\Klein\Request $request, \Klein\Response $response)
    {
        return $this->render('home.html.twig', [
            'message' => 'Home Page',
            'categories' => $this->categoryRepository->getCollection()->getItemsData()
        ]);
    }
}