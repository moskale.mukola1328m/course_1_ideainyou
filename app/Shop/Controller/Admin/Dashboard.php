<?php

namespace Shop\Controller\Admin;

use Shop\Model\CategoryRepository;
use Shop\Model\ProductRepository;

class Dashboard extends \Shop\Controller\Admin\AbstractController
{
    private $productRepository;
    private $categoryRepository;

    public function __construct(
        \Shop\Model\AdminLogin $adminLogin,
        ProductRepository $productRepository,
        CategoryRepository $categoryRepository
    ) {
        parent::__construct($adminLogin);
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function execute(\Klein\Request $request, \Klein\Response $response)
    {
        parent::execute($request, $response);
        return $this->render(
            'adminhtml/dashboard.html.twig', [
                'products' => $this->productRepository->getCollection()->getItemsData(),
                'categories' => $this->categoryRepository->getCollection()->getItemsData()
        ]);
    }
}