<?php

namespace Shop\Controller\Admin\Category;

use Shop\Model\CategoryRepository;
use Shop\Controller\Admin\AbstractController;

class Delete extends AbstractController
{
    private $categoryRepository;

    public function __construct(
        \Shop\Model\AdminLogin $adminLogin,
        CategoryRepository $categoryRepository
    ) {
        parent::__construct($adminLogin);
        $this->categoryRepository = $categoryRepository;
    }

    public function execute(\Klein\Request $request, \Klein\Response $response)
    {
        $id = $request->param('id');
        $this->categoryRepository->deleteById($id);

        $response->redirect('/admin/dashboard')->send();
    }
}