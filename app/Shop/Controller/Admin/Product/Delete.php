<?php

namespace Shop\Controller\Admin\Product;

use Shop\Model\ProductRepository as ProductRepositoryInterface;
use Shop\Controller\Admin\AbstractController;
use Shop\Model\Product;

class Delete extends AbstractController
{
    private $productRepository;

    public function __construct(
        \Shop\Model\AdminLogin $adminLogin,
        ProductRepositoryInterface $productRepository
    ) {
        parent::__construct($adminLogin);
        $this->productRepository = $productRepository;
    }

    public function execute(\Klein\Request $request, \Klein\Response $response)
    {
        $id = $request->param('id');
        $this->productRepository->deleteById($id);

        $response->redirect('/admin/dashboard')->send();
    }
}