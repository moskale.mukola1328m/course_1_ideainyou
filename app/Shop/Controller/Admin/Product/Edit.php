<?php

namespace Shop\Controller\Admin\Product;

use Shop\Controller\Admin\AbstractController;
use Shop\Exceptions\NotFoundProduct;
use Shop\Model\CategoryRepository;
use Shop\Model\ProductRepository;

class Edit extends AbstractController
{
    private $productRepository;
    private $categoryRepository;

    public function __construct(
        \Shop\Model\AdminLogin $adminLogin,
        ProductRepository $productRepository,
        CategoryRepository $categoryRepository
    ) {
        parent::__construct($adminLogin);
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function execute(\Klein\Request $request, \Klein\Response $response)
    {
        try {
            $product = $this->productRepository->getById($request->param('id'));
        } catch (NotFoundProduct $e) {
            return $response->redirect('/admin/dashboard')->send();
        }

        return $this->render('adminhtml/editProduct.html.twig', ['product' => $product->getData(), 'categories' => $this->categoryRepository->getCollection()->getItemsData()]);
    }
}