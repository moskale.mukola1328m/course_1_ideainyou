<?php

namespace Shop\Controller\Admin\Product;

use Shop\Controller\Admin\AbstractController;
use Shop\Model\CategoryRepository;

class AddAction extends AbstractController
{
    private $categoryRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        \Shop\Model\AdminLogin $adminLogin
    ) {
        parent::__construct($adminLogin);
        $this->categoryRepository = $categoryRepository;
    }

    public function execute(\Klein\Request $request, \Klein\Response $response)
    {
        parent::execute($request, $response);
        return $this->render('adminhtml/newProduct.html.twig', ['categories' => $this->categoryRepository->getCollection()->getItemsData()]);
    }
}