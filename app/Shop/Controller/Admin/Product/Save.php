<?php

namespace Shop\Controller\Admin\Product;

use Shop\Model\ProductRepository as ProductRepositoryInterface;
use Shop\Controller\Admin\AbstractController;
use Shop\Model\Product;

class Save extends AbstractController
{
    private $productRepository;

    public function __construct(
        \Shop\Model\AdminLogin $adminLogin,
        ProductRepositoryInterface $productRepository
    ) {
        parent::__construct($adminLogin);
        $this->productRepository = $productRepository;
    }

    public function execute(\Klein\Request $request, \Klein\Response $response)
    {
        $data = $request->paramsPost()->all();
        $product = new Product();
        $product->setData($data);
        $this->productRepository->save($product);

        $response->redirect('/admin/dashboard')->send();
    }
}