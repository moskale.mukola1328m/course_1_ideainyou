<?php

namespace Shop\Model;

use Shop\Api\ProductRepositoryInterface;
use Shop\Exceptions\NotFoundProduct;
use Shop\Service\Collection;
use Shop\Service\DataBase;

class ProductRepository implements ProductRepositoryInterface
{
    private $dataBase;

    public function __construct()
    {
        $this->dataBase = DataBase::getInstance();
    }

    public function save(Product $product): Product
    {
        $savedData = [];
        foreach (Product::FIELDS as $field) {
            $savedData[$field] = $product->getData($field);
        }

        if (!empty($savedData['entity_id'])) {
            $this->dataBase->update(Product::TABLE_NAME, $savedData, [
                'entity_id' => $savedData['entity_id']
            ]);
        } else {
            $this->dataBase->insert(Product::TABLE_NAME, $savedData);
        }
        return $product;
    }

    /**
     * @throws NotFoundProduct
     */
    public function getById(int $id): Product
    {
        $data = $this->dataBase->select(
            Product::TABLE_NAME, Product::FIELDS,
            [
                'entity_id' => $id
            ]
        );

        if (count($data)) {
            $data = array_shift($data);
            $product = new Product();
            $product->setData($data);
            return $product;
        }

        throw new NotFoundProduct('Product not found');
    }

    /**
     * @throws NotFoundProduct
     */
    public function deleteById(int $id): void
    {
        $data = $this->dataBase->delete(Product::TABLE_NAME, [
            'entity_id' => $id
        ]);

        if ($data->rowCount() == 0) {
            throw new NotFoundProduct('Product not found');
        }
    }

    public function getCollection(?array $condition = null): Collection
    {
        $data = $this->dataBase->select(Product::TABLE_NAME, Product::FIELDS, $condition);
        $_items = [];

        foreach ($data as $productData) {
            $product = new Product();
            $product->setData($productData);
            $_items[] = $product;
        }

        return new Collection($_items);
    }

    public function getTotalCount($categoryId = null): int
    {
        return (int) $this->dataBase->count(Product::TABLE_NAME, $categoryId ? ['category_id' => $categoryId] : null);
    }
}