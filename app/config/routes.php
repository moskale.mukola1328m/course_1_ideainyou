<?php

$routes = [
    [
        'method' => "GET",
        'path' => "/",
        'className' => \Shop\Controller\Home::class
    ],
    [
        'method' => "GET",
        'path' => "/admin",
        'className' => \Shop\Controller\AdminLogin::class
    ],
    [
        'method' => "POST",
        'path' => "/admin-login",
        'className' => \Shop\Controller\AdminLoginPost::class
    ],
    [
        'method' => "GET",
        'path' => "/admin/dashboard",
        'className' => \Shop\Controller\Admin\Dashboard::class
    ],
    [
        'method' => "GET",
        'path' => "/logout",
        'className' => \Shop\Controller\Logout::class
    ],
    [
        'method' => "GET",
        'path' => "/admin/product/add",
        'className' => \Shop\Controller\Admin\Product\AddAction::class
    ],
    [
        'method' => "POST",
        'path' => "/admin/product/save",
        'className' => \Shop\Controller\Admin\Product\Save::class
    ],
    [
        'method' => "GET",
        'path' => "/admin/product/edit/id/[i:id]",
        'className' => \Shop\Controller\Admin\Product\Edit::class
    ],
    [
        'method' => "GET",
        'path' => "/admin/product/delete/id/[i:id]",
        'className' => \Shop\Controller\Admin\Product\Delete::class
    ],
    [
        'method' => "GET",
        'path' => "/admin/category/add",
        'className' => \Shop\Controller\Admin\Category\AddAction::class
    ],
    [
        'method' => "POST",
        'path' => "/admin/category/save",
        'className' => \Shop\Controller\Admin\Category\Save::class
    ],
    [
        'method' => "GET",
        'path' => "/admin/category/edit/id/[i:id]",
        'className' => \Shop\Controller\Admin\Category\Edit::class
    ],
    [
        'method' => "GET",
        'path' => "/admin/category/delete/id/[i:id]",
        'className' => \Shop\Controller\Admin\Category\Delete::class
    ],
    [
        'method' => "GET",
        'path' => "/category/id/[i:id]",
        'className' => \Shop\Controller\Category::class
    ],
    [
        'method' => "GET",
        'path' => "/category/id/[i:id]/p/[i:p]",
        'className' => \Shop\Controller\Category::class
    ],
];
